package android.teramaet.crystalball;


public class Predictions {

    private static Predictions predictions;
    private String[] answers;

    private Predictions(){
        answers = new String[] {
                "Your wishes will come true.",
                "Brian will forget his computer tomorrow.",
                "Aaron will try out Synthol tomorrow.",
                "Your dreams won't come true",
                "You will meet your doom tomorrow",
                "Aaron will steal your microwave tomorrow"



        };
    }

    public static Predictions get(){
        if(predictions == null){
            predictions = new Predictions();
        }
        return predictions;
    }

    public String getPrediction(int randomNumber){

        return answers[randomNumber];
    }
}
