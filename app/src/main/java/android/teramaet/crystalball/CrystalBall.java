package android.teramaet.crystalball;

import android.content.Context;
        import android.graphics.drawable.AnimationDrawable;
        import android.hardware.Sensor;
        import android.hardware.SensorEvent;
        import android.hardware.SensorEventListener;
        import android.hardware.SensorManager;
        import android.media.Image;
        import android.media.MediaPlayer;
        import android.os.Handler;
        import android.support.v7.app.AppCompatActivity;
        import android.os.Bundle;
        import android.util.FloatMath;
        import android.view.View;
        import android.view.animation.Animation;
        import android.view.animation.AnimationUtils;
        import android.widget.ImageView;
        import android.widget.TextView;
        import android.widget.Toast;
        import java.util.ArrayList;
        import java.util.Collections;
        import java.util.List;
        import java.util.Random;

public class CrystalBall extends AppCompatActivity {

    private TextView answerText;

    private SensorManager sensorManager;
    private Sensor accelerometer;
    private float acceleration;
    private float currentAcceleration;
    private float previousAcceleration;
    public Animation animationslideintop;

    private final SensorEventListener sensorListener = new SensorEventListener() {
        @Override
        public void onSensorChanged(SensorEvent event) {
            float x = event.values[0];
            float y = event.values[1];
            float z = event.values[2];

            previousAcceleration = currentAcceleration;
            currentAcceleration = FloatMath.sqrt(x * x + y * y + z * z);
            float delta = currentAcceleration - previousAcceleration;
            acceleration = acceleration * 0.9f + delta;

            if(acceleration > 8) {
                Toast toast = Toast.makeText(getApplication(), "Device has been shaken", Toast.LENGTH_SHORT);
                toast.show();

                answerText = (TextView) findViewById(R.id.answerText);
                android.os.Handler textAnimation = new android.os.Handler();
                textAnimation.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        int randomNumber = (int) ((Math.random() * 5) +1 );
                        answerText = (TextView) findViewById(R.id.answerText);
                        answerText.setText(Predictions.get().getPrediction(randomNumber));
                        animationslideintop = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.abc_slide_in_top);
                        answerText.startAnimation(animationslideintop);
                        MediaPlayer mediaPlayer = MediaPlayer.create(CrystalBall.this, R.raw.crystal_ball);
                        mediaPlayer.start();


                        /*List<String> list = new ArrayList<String>();
                        list.add("Your wishes will come true.");
                        list.add("Brian will forget his computer tomorrow.");
                        list.add("Your luck will change tomorrow.");
                        list.add("");
                        Random rand = new Random();
                        String random = list.get(rand.nextInt(list.size()));

                        */
                    }


                }, 200);
                /*List<String> list = new ArrayList<String>();
                list.add("Your wishes will come true.");
                list.add("Brian will forget his computer tomorrow.");
                list.add("Your luck will change tomorrow.");
                list.add("");
                Random rand = new Random();
                String random = list.get(rand.nextInt(list.size()));*/
            }
        }

        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crystal_ball);

        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);


        acceleration = 0.0f;
        currentAcceleration = SensorManager.GRAVITY_EARTH;
        previousAcceleration = SensorManager.GRAVITY_EARTH;

        answerText = (TextView) findViewById(R.id.answerText);
        android.os.Handler textAnimation = new android.os.Handler();
        textAnimation.postDelayed(new Runnable() {
            @Override
            public void run() {
                animationslideintop = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.abc_slide_in_top);
                answerText.startAnimation(animationslideintop);

            }


        }, 200);

        MediaPlayer mediaPlayer = MediaPlayer.create(CrystalBall.this, R.raw.crystal_ball);
        mediaPlayer.start();


    }


    @Override
    protected void onResume() {
        super.onResume();
        sensorManager.registerListener(sensorListener, accelerometer, SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    protected void onPause() {
        super.onPause();
        sensorManager.unregisterListener(sensorListener);
    }
}


